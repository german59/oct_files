__author__ = ["Germán Pinedo"]
__copyright__ = "Copyright 2021, Germán Pinedo - CINVESTAV UNIDAD GUADALAJARA"
__credits__ = ["German Pinedo"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = ["German Pinedo"]
__email__ = "german.pinedo@cinvestav.mx"
__status__ = "Alpha"

from albumentations import HorizontalFlip, VerticalFlip, Rotate
import cv2
import os
import argparse
from skimage import transform
import numpy as np


def get_filenames(path):
    files = []
    ext = ('jpg', 'jpeg', 'png', 'tif', 'PNG', 'JPG', 'JPEG')
    for i in sorted(os.listdir(path)):
        if i.endswith(ext):
            files.append(os.path.join(path, i))
    return files


def plane_rotation(img, theta):
    aug = Rotate(limit=(theta, theta), p=1.0, border_mode=cv2.BORDER_CONSTANT, value=0)
    augmented = aug(image=img)
    img1 = augmented["image"]
    return img1


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', help='Image path', required=True)
    parser.add_argument('-d', help='degrees', type=int, required=False)
    args = parser.parse_args()
    image_file = args.i
    degrees = args.d
    img = cv2.imread(image_file)
    img_rotated = plane_rotation(img, degrees)
    cv2.imwrite('test_rotated.png', img_rotated)


if __name__ == '__main__':
    main()
