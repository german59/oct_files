"""
**************************************************************************************************************
# credits https://github.com/asp1420/A-Bio-inspired-Quaternion-Local-Phase-CNN/blob/master/tools/transform.py
**************************************************************************************************************
"""
from matplotlib import pyplot as plt
import numpy as np
import cv2
import argparse

def qgabor(L1, L2, phi=0, ch=2, cv=2, sh=2, sv=2):
    X = np.arange(-L1 / 2, L1 / 2, 1)
    Y = np.arange(-L2 / 2, L2 / 2, 1)
    X, Y = np.meshgrid(X, Y)
    x = X * np.cos(phi) + Y * np.sin(phi)
    y = -X * np.sin(phi) + Y * np.cos(phi)
    g = np.exp((-(x ** 2)) / (2 * sh ** 2)) * np.exp((-(y ** 2)) / (2 * sv ** 2))
    r = g * np.cos(ch * x / sh) * np.cos(cv * y / sv)
    i = g * np.sin(ch * x / sh) * np.cos(cv * y / sv)
    j = g * np.cos(ch * x / sh) * np.sin(cv * y / sv)
    k = g * np.sin(ch * x / sh) * np.sin(cv * y / sv)
    return r, i, j, k


def qfilter(img, L1, L2, phi=0, ch=2, cv=2, sh=2, sv=2):
    b = 1
    img = cv2.add(img, b)  # some bias to avoid zero in the artag
    dft = cv2.dft(np.float32(img), flags=cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)
    # magnitude_spec = 20 * np.log((cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1])))
    r, i, j, k = qgabor(L1, L2, phi, ch, cv, sh, sv)
    m0 = np.dstack((r, r))
    m1 = np.dstack((i, i))
    m2 = np.dstack((j, j))
    m3 = np.dstack((k, k))
    fshift0 = dft_shift * m0
    fshift1 = dft_shift * m1
    fshift2 = dft_shift * m2
    fshift3 = dft_shift * m3

    f_ishift0 = np.fft.ifftshift(fshift0)
    img_back0 = cv2.idft(f_ishift0)
    img_back0 = cv2.magnitude(img_back0[:, :, 0], img_back0[:, :, 1])

    f_ishift1 = np.fft.ifftshift(fshift1)
    img_back1 = cv2.idft(f_ishift1)
    img_back1 = cv2.magnitude(img_back1[:, :, 0], img_back1[:, :, 1])

    f_ishift2 = np.fft.ifftshift(fshift2)
    img_back2 = cv2.idft(f_ishift2)
    img_back2 = cv2.magnitude(img_back2[:, :, 0], img_back2[:, :, 1])

    f_ishift3 = np.fft.ifftshift(fshift3)
    img_back3 = cv2.idft(f_ishift3)
    img_back3 = cv2.magnitude(img_back3[:, :, 0], img_back3[:, :, 1])

    return img_back0, img_back1, img_back2, img_back3

parser = argparse.ArgumentParser()
parser.add_argument('-i', help='Image path', required=True)
parser.add_argument('-d', help='degrees', type=int, required=False)
args = parser.parse_args()
image_file = args.i
degrees = args.d
img = cv2.imread(image_file, 0)
img_array = np.array(img)
h, w = img.shape
axis = 0
b = abs(int(h - w))
b1 = int(b / 2)
b2 = b - b1
if h > w:
    axis = 1
    z1 = np.zeros((h, b1))
    z2 = np.zeros((h, b2))
elif w > h:
    z1 = np.zeros((b1, w))
    z2 = np.zeros((b2, w))
img_array = np.append(img, z1, axis=axis)
img_array = np.append(z2, img_array, axis=axis)

sh = 20
sv = 8
ch = 1
cv = 3
phi = degrees

r, i, j, k = qgabor(51, 51, phi, ch, cv, sh, sv) #
img_back0, img_back1, img_back2, img_back3 = qfilter(img_array, img_array.shape[0], img_array.shape[1], phi, ch, cv, sh, sv)
image_quaternion = np.stack((img_back0, img_back1, img_back2, img_back3),axis=-1)
print(image_quaternion.shape)
fig = plt.figure()
ax1 = fig.add_subplot(341)
ax2 = fig.add_subplot(342)
ax3 = fig.add_subplot(343)
ax4 = fig.add_subplot(344)
ax5 = fig.add_subplot(345)
ax6 = fig.add_subplot(346)
ax7 = fig.add_subplot(347)
ax8 = fig.add_subplot(348)
ax9 = fig.add_subplot(325)
ax10 = fig.add_subplot(326)
ax1.imshow(r, cmap='gray')
ax2.imshow(i, cmap='gray')
ax3.imshow(j, cmap='gray')
ax4.imshow(k, cmap='gray')
ax5.imshow(img_back0, cmap='gray')
ax6.imshow(img_back1, cmap='gray')
ax7.imshow(img_back2, cmap='gray')
ax8.imshow(img_back3, cmap='gray')
ax9.imshow(img, cmap='gray')
ax10.imshow(img_array, cmap='gray')
ax1.set_axis_off()
ax2.set_axis_off()
ax3.set_axis_off()
ax4.set_axis_off()
ax5.set_axis_off()
ax6.set_axis_off()
ax7.set_axis_off()
ax8.set_axis_off()
ax9.set_axis_off()
ax10.set_axis_off()
ax1.set_title('r gabor')
ax2.set_title('i gabor')
ax3.set_title('j gabor')
ax4.set_title('k gabor')
ax5.set_title('r-new-img')
ax6.set_title('i-new-img')
ax7.set_title('j-new-img')
ax8.set_title('k-new-img')
ax9.set_title('Original')
ax10.set_title('Square')
plt.show()
