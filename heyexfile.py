from matplotlib import pyplot as plt
import multiprocessing as mp
import heyexReader
import glob
import numpy as np
import os
import cv2

def open_vol_file(filename):
    vol = heyexReader.volFile(filename)
    split_test = os.path.splitext(filename)
    vol.renderIRslo("SLO/SLO_"+os.path.basename(split_test[0])+".png", renderGrid = True)
    try:
        vol.renderOCTscans("renderOCT/OCT_"+os.path.basename(split_test[0]), renderSeg = False)
    except:
        print("No segmentation data")
        pass
    print(filename)



def get_filenames(path):
    X0 = []
    ext = ('vol')
    for i in sorted(os.listdir(path)):
        if i.endswith(ext):
            X0.append(os.path.join(path, i))
    return X0

def main():
    data_path = "OCT1/"
    pool = mp.Pool(processes=mp.cpu_count())
    files = get_filenames(data_path)
    #open_vol_file(files[0])
    # for i in files:
    #     try:
    #         open_vol_file(i)
    #     except:
    #         pass
    #pool.map(open_vol_file, files)
    os.mkdir("SLO")
    os.mkdir("renderOCT")
    try:
        pool.map(open_vol_file, files)
    except:
        pass
    #print(type(files))
    # image = "OCT1/Anonym_2_1920.vol"
    # data = ep.Oct.from_heyex_vol(image)
    # plt.figure()
    # data.plot()
    # plt.figure()
    # data.bscans[0].plot()
    # plt.show()

if __name__ == '__main__':
    main()
